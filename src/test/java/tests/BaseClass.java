package tests;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.* ;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseClass {
public  AndroidDriver driver;
@BeforeTest	
	public void setup()  {
	try
	{
	DesiredCapabilities dc = new DesiredCapabilities();
	
	dc.setCapability(MobileCapabilityType.AUTOMATION_NAME,"Appium");
	
	dc.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
	dc.setCapability(MobileCapabilityType.PLATFORM_VERSION,11.0);
	
	dc.setCapability(MobileCapabilityType.DEVICE_NAME,"Android");
	dc.setCapability(MobileCapabilityType.BROWSER_NAME,"chrome");
	
	
	URL url=new URL("http://127.0.0.1:4723/wd/hub");	
	driver= new AndroidDriver(url,dc);
	}

catch(Exception exp){
	System.out.println("the cause is:" +exp.getCause());
	System.out.println("the message is:" +exp.getMessage());
	exp.printStackTrace();
	
	
}

}
@Test
public void sampleTest() {
System.out.print("****"
		+ "++++++++KI"); 	
}
@AfterTest
public void teardown() {
driver.quit();	
}


}




